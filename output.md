# output (no trace)

```shell
emptyList = LinkedList.makeList();
-> []

list = LinkedList.makeNode("One", LinkedList.makeNode("Two"));
-> [ One | Two ]

secondList = LinkedList.makeList("Three", "Four");
-> [ Three | Four ]

Concatenation
----------------------------------
list = list.concat(secondList);
-> [ One | Two | Three | Four ]

Size
----------------------------------
list.size(): 4
list.tail().size(): 3
list.tail().tail().size(): 2
list.tail().tail().tail().size(): 1
list.tail().tail().tail().tail().size(): 0
list.tail().tail().tail().tail().tail().size(): 0     # beyond last node

Slice
----------------------------------
slice(0, 0): []
slice(0, 1): [ One ]
slice(0, 2): [ One | Two ]
slice(0, 3): [ One | Two | Three ]
slice(0, 4): [ One | Two | Three | Four ]
slice(1, 1): [ Two ]
slice(1, 2): [ Two | Three ]
slice(1, 3): [ Two | Three | Four ]
slice(2, 1): [ Three ]
slice(2, 2): [ Three | Four ]
slice(3, 1): [ Four ]
tail.slice(0, 0): []
tail.slice(0, 1): [ Two ]
tail.slice(0, 2): [ Two | Three ]
tail.slice(0, 3): [ Two | Three | Four ]
tail.tail.slice(0, 1): [ Three ]
tail.tail.slice(0, 2): [ Three | Four ]
tail.tail.slice(0, 3): [ Three | Four ]            # beyond last node

Search
----------------------------------
list.search("One"): [ One | Two | Three | Four ]
list.search("Three"): [ Three | Four ]
list.search("Five"): []
list.search("One").value(): One
list.search("Five").value(): null
list.search(null): []
list.search(null).value(): null

Unique
----------------------------------
[ One | Two | Three | Four | One | Two | Three | Four ].unique(): [ One | Two | Three | Four ]

Append
----------------------------------
[].append("One"): [ One ]
[ One | Two | Three | Four ].append("Five"): [ One | Two | Three | Four | Five ]

Insert(String)
----------------------------------
[ One | Two | Three | Four ].insert("Zero", 0): [ Zero | One | Two | Three | Four ]
[ One | Two | Three | Four ].insert("1.5", 1): [ One | 1.5 | Two | Three | Four ]
[ One | Two | Three | Four ].insert("2.5", 2): [ One | Two | 2.5 | Three | Four ]
[ One | Two | Three | Four ].insert("3.5", 3): [ One | Two | Three | 3.5 | Four ]
[ One | Two | Three | Four ].insert("5", 4): [ One | Two | Three | Four | 5 ]
[ One | Two | Three | Four ].insert("5", 5): [ One | Two | Three | Four | 5 ]     # beyond last node

Insert(LinkedList)
----------------------------------
[ One | Two | Three | Four ].insert([ Elem1 | Elem2 ], 0): [ Elem1 | Elem2 | One | Two | Three | Four ]
[ One | Two | Three | Four ].insert([ Elem1 | Elem2 ], 2): [ One | Two | Elem1 | Elem2 | Three | Four ]
[ One | Two | Three | Four ].insert([ Elem1 | Elem2 ], 4): [ One | Two | Three | Four | Elem1 | Elem2 ]

Invert
----------------------------------
list.invert(): [ Four | Three | Two | One ]
list.tail().invert(): [ Four | Three | Two ]
list.tail().tail().invert(): [ Four | Three ]
list.tail().tail().tail().invert(): [ Four ]
list.tail().tail().tail().tail().invert(): []            # invert empty node
list.tail().tail().tail().tail().tail().invert(): []     # beyond last node

Includes
----------------------------------
[].includes([]): true
[ One | Two | Three | Four ].includes([]): true
[ One | Two | Three | Four ].includes([ One | Two | Three | Four ]): true
[ One | Two | Three | Four ].includes([ One ]): true
[ One | Two | Three | Four ].includes([ Two ]): true
[ One | Two | Three | Four ].includes([ Two | Three ]): true
[ One | Two | Three | Four ].includes([ Two | Three | Four ]): true
[ One | Two | Three | Four ].includes([ Three | Four ]): true
[ One | Two | Three | Four ].includes([ Four ]): true
[ One | Two | Three | Four | Five | Six ].includes([ One | Two | Three | Four ]): true
[ One | Two | Three | Four ].includes([ One | Two | Three | Four | Five | Six ]): false
[ One | Two | Three | Four ].includes([ Five | Six ]): false

Intersection
----------------------------------
[].intersection([]): []
[ One | Two | Three | Four ].intersection([]): []
[ One | Two | Three | Four ].intersection([ One ]): [ One ]
[ One | Two | Three | Four ].intersection([ Three | Four ]): [ Three | Four ]

Union
----------------------------------
[ One | Two | Three | Four ].union([]): [ One | Two | Three | Four ]
[ One | Two | Three | Four ].union([ Three | Four ]): [ One | Two | Three | Four ]
[ One | Two | Three | Four ].union([ One | Five | Six ]): [ One | Two | Three | Four | Five | Six ]

Iterable
----------------------------------
ONE \ TWO \ THREE \ FOUR \     # iterable for loop (toUpperCase())
one \ two \ three \ four \     # forEach loop (toLowerCase())

Mutation (in place)
----------------------------------
for (LinkedList node : list) {
    node.setValue((++i).toString());
}
list: [ 1 | 2 | 3 | 4 ]
```



# output (with trace)

```shell
emptyList = LinkedList.makeList();
-> []

list = LinkedList.makeNode("One", LinkedList.makeNode("Two"));
-> [ One | Two ]
<trace> [ Three ].copy()

secondList = LinkedList.makeList("Three", "Four");
-> [ Three | Four ]

Concatenation
----------------------------------
<trace> [ One | Two ].concat([ Three | Four ])
<trace> [ Two ].concat([ Three | Four ])
<trace> [ Three | Four ].copy()
<trace> [ Four ].copy()
list = list.concat(secondList);
-> [ One | Two | Three | Four ]

Size
----------------------------------
list.size(): 4
list.tail().size(): 3
list.tail().tail().size(): 2
list.tail().tail().tail().size(): 1
list.tail().tail().tail().tail().size(): 0
list.tail().tail().tail().tail().tail().size(): 0     # beyond last node

Slice
----------------------------------
<trace> _slice(0, 0, 0)
slice(0, 0): []
<trace> _slice(0, 1, 0)
<trace> _slice(0, 1, 1)
slice(0, 1): [ One ]
<trace> _slice(0, 2, 0)
<trace> _slice(0, 2, 1)
<trace> _slice(0, 2, 2)
slice(0, 2): [ One | Two ]
<trace> _slice(0, 3, 0)
<trace> _slice(0, 3, 1)
<trace> _slice(0, 3, 2)
<trace> _slice(0, 3, 3)
slice(0, 3): [ One | Two | Three ]
<trace> _slice(0, 4, 0)
<trace> _slice(0, 4, 1)
<trace> _slice(0, 4, 2)
<trace> _slice(0, 4, 3)
slice(0, 4): [ One | Two | Three | Four ]
<trace> _slice(1, 1, 0)
<trace> _slice(1, 1, 1)
<trace> _slice(1, 1, 2)
slice(1, 1): [ Two ]
<trace> _slice(1, 2, 0)
<trace> _slice(1, 2, 1)
<trace> _slice(1, 2, 2)
<trace> _slice(1, 2, 3)
slice(1, 2): [ Two | Three ]
<trace> _slice(1, 3, 0)
<trace> _slice(1, 3, 1)
<trace> _slice(1, 3, 2)
<trace> _slice(1, 3, 3)
slice(1, 3): [ Two | Three | Four ]
<trace> _slice(2, 1, 0)
<trace> _slice(2, 1, 1)
<trace> _slice(2, 1, 2)
<trace> _slice(2, 1, 3)
slice(2, 1): [ Three ]
<trace> _slice(2, 2, 0)
<trace> _slice(2, 2, 1)
<trace> _slice(2, 2, 2)
<trace> _slice(2, 2, 3)
slice(2, 2): [ Three | Four ]
<trace> _slice(3, 1, 0)
<trace> _slice(3, 1, 1)
<trace> _slice(3, 1, 2)
<trace> _slice(3, 1, 3)
slice(3, 1): [ Four ]
<trace> _slice(0, 0, 0)
tail.slice(0, 0): []
<trace> _slice(0, 1, 0)
<trace> _slice(0, 1, 1)
tail.slice(0, 1): [ Two ]
<trace> _slice(0, 2, 0)
<trace> _slice(0, 2, 1)
<trace> _slice(0, 2, 2)
tail.slice(0, 2): [ Two | Three ]
<trace> _slice(0, 3, 0)
<trace> _slice(0, 3, 1)
<trace> _slice(0, 3, 2)
tail.slice(0, 3): [ Two | Three | Four ]
<trace> _slice(0, 1, 0)
<trace> _slice(0, 1, 1)
tail.tail.slice(0, 1): [ Three ]
<trace> _slice(0, 2, 0)
<trace> _slice(0, 2, 1)
tail.tail.slice(0, 2): [ Three | Four ]
<trace> _slice(0, 3, 0)
<trace> _slice(0, 3, 1)
tail.tail.slice(0, 3): [ Three | Four ]            # beyond last node

Search
----------------------------------
list.search("One"): [ One | Two | Three | Four ]
list.search("Three"): [ Three | Four ]
list.search("Five"): []
list.search("One").value(): One
list.search("Five").value(): null
list.search(null): []
list.search(null).value(): null

Unique
----------------------------------
<trace> [ One | Two | Three | Four ].concat([ One | Two | Three | Four ])
<trace> [ Two | Three | Four ].concat([ One | Two | Three | Four ])
<trace> [ Three | Four ].concat([ One | Two | Three | Four ])
<trace> [ Four ].concat([ One | Two | Three | Four ])
<trace> [ One | Two | Three | Four ].copy()
<trace> [ Two | Three | Four ].copy()
<trace> [ Three | Four ].copy()
<trace> [ Four ].copy()
<trace> [ One | Two | Three | Four ].concat([ One | Two | Three | Four ])
<trace> [ Two | Three | Four ].concat([ One | Two | Three | Four ])
<trace> [ Three | Four ].concat([ One | Two | Three | Four ])
<trace> [ Four ].concat([ One | Two | Three | Four ])
<trace> [ One | Two | Three | Four ].copy()
<trace> [ Two | Three | Four ].copy()
<trace> [ Three | Four ].copy()
<trace> [ Four ].copy()
<trace> [ One | Two | Three | Four | One | Two | Three | Four ]._unique([])
<trace> [ One ].copy()
<trace> [ Two | Three | Four | One | Two | Three | Four ]._unique([ One ])
<trace> [ One ].concat([ Two ])
<trace> [ Two ].copy()
<trace> [ Three | Four | One | Two | Three | Four ]._unique([ One | Two ])
<trace> [ One | Two ].concat([ Three ])
<trace> [ Two ].concat([ Three ])
<trace> [ Three ].copy()
<trace> [ Four | One | Two | Three | Four ]._unique([ One | Two | Three ])
<trace> [ One | Two | Three ].concat([ Four ])
<trace> [ Two | Three ].concat([ Four ])
<trace> [ Three ].concat([ Four ])
<trace> [ Four ].copy()
<trace> [ One | Two | Three | Four ]._unique([ One | Two | Three | Four ])
<trace> [ Two | Three | Four ]._unique([ One | Two | Three | Four ])
<trace> [ Three | Four ]._unique([ One | Two | Three | Four ])
<trace> [ Four ]._unique([ One | Two | Three | Four ])
[ One | Two | Three | Four | One | Two | Three | Four ].unique(): [ One | Two | Three | Four ]

Append
----------------------------------
[].append("One"): [ One ]
<trace> [ One | Two | Three | Four ].concat([ Five ])
<trace> [ Two | Three | Four ].concat([ Five ])
<trace> [ Three | Four ].concat([ Five ])
<trace> [ Four ].concat([ Five ])
<trace> [ Five ].copy()
[ One | Two | Three | Four ].append("Five"): [ One | Two | Three | Four | Five ]

Insert(String)
----------------------------------
<trace> [ One | Two | Three | Four ].copy()
<trace> [ Two | Three | Four ].copy()
<trace> [ Three | Four ].copy()
<trace> [ Four ].copy()
[ One | Two | Three | Four ].insert("Zero", 0): [ Zero | One | Two | Three | Four ]
<trace> [ One | Two | Three | Four ].copy()
<trace> [ Two | Three | Four ].copy()
<trace> [ Three | Four ].copy()
<trace> [ Four ].copy()
[ One | Two | Three | Four ].insert("1.5", 1): [ One | 1.5 | Two | Three | Four ]
<trace> [ One | Two | Three | Four ].copy()
<trace> [ Two | Three | Four ].copy()
<trace> [ Three | Four ].copy()
<trace> [ Four ].copy()
[ One | Two | Three | Four ].insert("2.5", 2): [ One | Two | 2.5 | Three | Four ]
<trace> [ One | Two | Three | Four ].copy()
<trace> [ Two | Three | Four ].copy()
<trace> [ Three | Four ].copy()
<trace> [ Four ].copy()
[ One | Two | Three | Four ].insert("3.5", 3): [ One | Two | Three | 3.5 | Four ]
<trace> [ One | Two | Three | Four ].copy()
<trace> [ Two | Three | Four ].copy()
<trace> [ Three | Four ].copy()
<trace> [ Four ].copy()
[ One | Two | Three | Four ].insert("5", 4): [ One | Two | Three | Four | 5 ]
<trace> [ One | Two | Three | Four ].copy()
<trace> [ Two | Three | Four ].copy()
<trace> [ Three | Four ].copy()
<trace> [ Four ].copy()
[ One | Two | Three | Four ].insert("5", 5): [ One | Two | Three | Four | 5 ]     # beyond last node

Insert(LinkedList)
----------------------------------
<trace> [ Elem1 ].copy()
<trace> [ Elem1 ].copy()
<trace> _slice(0, 0, 0)
<trace> [ Elem1 | Elem2 ].copy()
<trace> [ Elem2 ].copy()
<trace> _slice(0, 4, 0)
<trace> _slice(0, 4, 1)
<trace> _slice(0, 4, 2)
<trace> _slice(0, 4, 3)
<trace> [ Elem1 | Elem2 ].concat([ One | Two | Three | Four ])
<trace> [ Elem2 ].concat([ One | Two | Three | Four ])
<trace> [ One | Two | Three | Four ].copy()
<trace> [ Two | Three | Four ].copy()
<trace> [ Three | Four ].copy()
<trace> [ Four ].copy()
[ One | Two | Three | Four ].insert([ Elem1 | Elem2 ], 0): [ Elem1 | Elem2 | One | Two | Three | Four ]
<trace> [ Elem1 ].copy()
<trace> [ Elem1 ].copy()
<trace> _slice(0, 2, 0)
<trace> _slice(0, 2, 1)
<trace> _slice(0, 2, 2)
<trace> [ One | Two ].concat([ Elem1 | Elem2 ])
<trace> [ Two ].concat([ Elem1 | Elem2 ])
<trace> [ Elem1 | Elem2 ].copy()
<trace> [ Elem2 ].copy()
<trace> _slice(2, 4, 0)
<trace> _slice(2, 4, 1)
<trace> _slice(2, 4, 2)
<trace> _slice(2, 4, 3)
<trace> [ One | Two | Elem1 | Elem2 ].concat([ Three | Four ])
<trace> [ Two | Elem1 | Elem2 ].concat([ Three | Four ])
<trace> [ Elem1 | Elem2 ].concat([ Three | Four ])
<trace> [ Elem2 ].concat([ Three | Four ])
<trace> [ Three | Four ].copy()
<trace> [ Four ].copy()
[ One | Two | Three | Four ].insert([ Elem1 | Elem2 ], 2): [ One | Two | Elem1 | Elem2 | Three | Four ]
<trace> [ Elem1 ].copy()
<trace> [ Elem1 ].copy()
<trace> _slice(0, 4, 0)
<trace> _slice(0, 4, 1)
<trace> _slice(0, 4, 2)
<trace> _slice(0, 4, 3)
<trace> [ One | Two | Three | Four ].concat([ Elem1 | Elem2 ])
<trace> [ Two | Three | Four ].concat([ Elem1 | Elem2 ])
<trace> [ Three | Four ].concat([ Elem1 | Elem2 ])
<trace> [ Four ].concat([ Elem1 | Elem2 ])
<trace> [ Elem1 | Elem2 ].copy()
<trace> [ Elem2 ].copy()
<trace> _slice(4, 4, 0)
<trace> _slice(4, 4, 1)
<trace> _slice(4, 4, 2)
<trace> _slice(4, 4, 3)
<trace> [ One | Two | Three | Four | Elem1 | Elem2 ].concat([])
<trace> [ Two | Three | Four | Elem1 | Elem2 ].concat([])
<trace> [ Three | Four | Elem1 | Elem2 ].concat([])
<trace> [ Four | Elem1 | Elem2 ].concat([])
<trace> [ Elem1 | Elem2 ].concat([])
<trace> [ Elem2 ].concat([])
[ One | Two | Three | Four ].insert([ Elem1 | Elem2 ], 4): [ One | Two | Three | Four | Elem1 | Elem2 ]

Invert
----------------------------------
<trace> [ Four ].concat([ Three ])
<trace> [ Three ].copy()
<trace> [ Four | Three ].concat([ Two ])
<trace> [ Three ].concat([ Two ])
<trace> [ Two ].copy()
<trace> [ Four | Three | Two ].concat([ One ])
<trace> [ Three | Two ].concat([ One ])
<trace> [ Two ].concat([ One ])
<trace> [ One ].copy()
list.invert(): [ Four | Three | Two | One ]
<trace> [ Four ].concat([ Three ])
<trace> [ Three ].copy()
<trace> [ Four | Three ].concat([ Two ])
<trace> [ Three ].concat([ Two ])
<trace> [ Two ].copy()
list.tail().invert(): [ Four | Three | Two ]
<trace> [ Four ].concat([ Three ])
<trace> [ Three ].copy()
list.tail().tail().invert(): [ Four | Three ]
list.tail().tail().tail().invert(): [ Four ]
list.tail().tail().tail().tail().invert(): []            # invert empty node
list.tail().tail().tail().tail().tail().invert(): []     # beyond last node

Includes
----------------------------------
[].includes([]): true
[ One | Two | Three | Four ].includes([]): true
[ One | Two | Three | Four ].includes([ One | Two | Three | Four ]): true
<trace> _slice(0, 1, 0)
<trace> _slice(0, 1, 1)
<trace> _slice(0, 1, 0)
<trace> _slice(0, 1, 1)
[ One | Two | Three | Four ].includes([ One ]): true
<trace> _slice(1, 1, 0)
<trace> _slice(1, 1, 1)
<trace> _slice(1, 1, 2)
<trace> _slice(0, 1, 0)
<trace> _slice(0, 1, 1)
[ One | Two | Three | Four ].includes([ Two ]): true
<trace> _slice(1, 2, 0)
<trace> _slice(1, 2, 1)
<trace> _slice(1, 2, 2)
<trace> _slice(1, 2, 3)
<trace> _slice(0, 1, 0)
<trace> _slice(0, 1, 1)
[ One | Two | Three | Four ].includes([ Two | Three ]): true
<trace> _slice(1, 3, 0)
<trace> _slice(1, 3, 1)
<trace> _slice(1, 3, 2)
<trace> _slice(1, 3, 3)
<trace> _slice(1, 3, 0)
<trace> _slice(1, 3, 1)
<trace> _slice(1, 3, 2)
<trace> _slice(1, 3, 3)
[ One | Two | Three | Four ].includes([ Two | Three | Four ]): true
<trace> _slice(2, 2, 0)
<trace> _slice(2, 2, 1)
<trace> _slice(2, 2, 2)
<trace> _slice(2, 2, 3)
<trace> _slice(2, 2, 0)
<trace> _slice(2, 2, 1)
<trace> _slice(2, 2, 2)
<trace> _slice(2, 2, 3)
[ One | Two | Three | Four ].includes([ Three | Four ]): true
<trace> _slice(3, 1, 0)
<trace> _slice(3, 1, 1)
<trace> _slice(3, 1, 2)
<trace> _slice(3, 1, 3)
<trace> _slice(2, 2, 0)
<trace> _slice(2, 2, 1)
<trace> _slice(2, 2, 2)
<trace> _slice(2, 2, 3)
[ One | Two | Three | Four ].includes([ Four ]): true
<trace> [ Five ].copy()
<trace> [ One | Two | Three | Four ].concat([ Five | Six ])
<trace> [ Two | Three | Four ].concat([ Five | Six ])
<trace> [ Three | Four ].concat([ Five | Six ])
<trace> [ Four ].concat([ Five | Six ])
<trace> [ Five | Six ].copy()
<trace> [ Six ].copy()
<trace> [ Five ].copy()
<trace> [ One | Two | Three | Four ].concat([ Five | Six ])
<trace> [ Two | Three | Four ].concat([ Five | Six ])
<trace> [ Three | Four ].concat([ Five | Six ])
<trace> [ Four ].concat([ Five | Six ])
<trace> [ Five | Six ].copy()
<trace> [ Six ].copy()
[ One | Two | Three | Four | Five | Six ].includes([ One | Two | Three | Four ]): true
<trace> [ Five ].copy()
<trace> [ One | Two | Three | Four ].concat([ Five | Six ])
<trace> [ Two | Three | Four ].concat([ Five | Six ])
<trace> [ Three | Four ].concat([ Five | Six ])
<trace> [ Four ].concat([ Five | Six ])
<trace> [ Five | Six ].copy()
<trace> [ Six ].copy()
<trace> [ Five ].copy()
<trace> [ One | Two | Three | Four ].concat([ Five | Six ])
<trace> [ Two | Three | Four ].concat([ Five | Six ])
<trace> [ Three | Four ].concat([ Five | Six ])
<trace> [ Four ].concat([ Five | Six ])
<trace> [ Five | Six ].copy()
<trace> [ Six ].copy()
[ One | Two | Three | Four ].includes([ One | Two | Three | Four | Five | Six ]): false
<trace> [ Five ].copy()
<trace> [ Five ].copy()
[ One | Two | Three | Four ].includes([ Five | Six ]): false

Intersection
----------------------------------
[].intersection([]): []
[ One | Two | Three | Four ].intersection([]): []
[ One | Two | Three | Four ].intersection([ One ]): [ One ]
<trace> [ Three ].concat([ Four ])
<trace> [ Four ].copy()
[ One | Two | Three | Four ].intersection([ Three | Four ]): [ Three | Four ]

Union
----------------------------------
<trace> [ One | Two | Three | Four ].concat([])
<trace> [ Two | Three | Four ].concat([])
<trace> [ Three | Four ].concat([])
<trace> [ Four ].concat([])
<trace> [ One | Two | Three | Four ]._unique([])
<trace> [ One ].copy()
<trace> [ Two | Three | Four ]._unique([ One ])
<trace> [ One ].concat([ Two ])
<trace> [ Two ].copy()
<trace> [ Three | Four ]._unique([ One | Two ])
<trace> [ One | Two ].concat([ Three ])
<trace> [ Two ].concat([ Three ])
<trace> [ Three ].copy()
<trace> [ Four ]._unique([ One | Two | Three ])
<trace> [ One | Two | Three ].concat([ Four ])
<trace> [ Two | Three ].concat([ Four ])
<trace> [ Three ].concat([ Four ])
<trace> [ Four ].copy()
[ One | Two | Three | Four ].union([]): [ One | Two | Three | Four ]
<trace> [ One | Two | Three | Four ].concat([ Three | Four ])
<trace> [ Two | Three | Four ].concat([ Three | Four ])
<trace> [ Three | Four ].concat([ Three | Four ])
<trace> [ Four ].concat([ Three | Four ])
<trace> [ Three | Four ].copy()
<trace> [ Four ].copy()
<trace> [ One | Two | Three | Four | Three | Four ]._unique([])
<trace> [ One ].copy()
<trace> [ Two | Three | Four | Three | Four ]._unique([ One ])
<trace> [ One ].concat([ Two ])
<trace> [ Two ].copy()
<trace> [ Three | Four | Three | Four ]._unique([ One | Two ])
<trace> [ One | Two ].concat([ Three ])
<trace> [ Two ].concat([ Three ])
<trace> [ Three ].copy()
<trace> [ Four | Three | Four ]._unique([ One | Two | Three ])
<trace> [ One | Two | Three ].concat([ Four ])
<trace> [ Two | Three ].concat([ Four ])
<trace> [ Three ].concat([ Four ])
<trace> [ Four ].copy()
<trace> [ Three | Four ]._unique([ One | Two | Three | Four ])
<trace> [ Four ]._unique([ One | Two | Three | Four ])
[ One | Two | Three | Four ].union([ Three | Four ]): [ One | Two | Three | Four ]
<trace> [ One ].copy()
<trace> [ One | Five ].copy()
<trace> [ Five ].copy()
<trace> [ One ].copy()
<trace> [ One | Five ].copy()
<trace> [ Five ].copy()
<trace> [ One | Two | Three | Four ].concat([ One | Five | Six ])
<trace> [ Two | Three | Four ].concat([ One | Five | Six ])
<trace> [ Three | Four ].concat([ One | Five | Six ])
<trace> [ Four ].concat([ One | Five | Six ])
<trace> [ One | Five | Six ].copy()
<trace> [ Five | Six ].copy()
<trace> [ Six ].copy()
<trace> [ One | Two | Three | Four | One | Five | Six ]._unique([])
<trace> [ One ].copy()
<trace> [ Two | Three | Four | One | Five | Six ]._unique([ One ])
<trace> [ One ].concat([ Two ])
<trace> [ Two ].copy()
<trace> [ Three | Four | One | Five | Six ]._unique([ One | Two ])
<trace> [ One | Two ].concat([ Three ])
<trace> [ Two ].concat([ Three ])
<trace> [ Three ].copy()
<trace> [ Four | One | Five | Six ]._unique([ One | Two | Three ])
<trace> [ One | Two | Three ].concat([ Four ])
<trace> [ Two | Three ].concat([ Four ])
<trace> [ Three ].concat([ Four ])
<trace> [ Four ].copy()
<trace> [ One | Five | Six ]._unique([ One | Two | Three | Four ])
<trace> [ Five | Six ]._unique([ One | Two | Three | Four ])
<trace> [ One | Two | Three | Four ].concat([ Five ])
<trace> [ Two | Three | Four ].concat([ Five ])
<trace> [ Three | Four ].concat([ Five ])
<trace> [ Four ].concat([ Five ])
<trace> [ Five ].copy()
<trace> [ Six ]._unique([ One | Two | Three | Four | Five ])
<trace> [ One | Two | Three | Four | Five ].concat([ Six ])
<trace> [ Two | Three | Four | Five ].concat([ Six ])
<trace> [ Three | Four | Five ].concat([ Six ])
<trace> [ Four | Five ].concat([ Six ])
<trace> [ Five ].concat([ Six ])
<trace> [ Six ].copy()
[ One | Two | Three | Four ].union([ One | Five | Six ]): [ One | Two | Three | Four | Five | Six ]

Iterable
----------------------------------
ONE \ TWO \ THREE \ FOUR \     # iterable for loop (toUpperCase())
one \ two \ three \ four \     # forEach loop (toLowerCase())

Mutation (in place)
----------------------------------
for (LinkedList node : list) {
    node.setValue((++i).toString());
}
list: [ 1 | 2 | 3 | 4 ]
```
