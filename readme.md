# TP1 Linked List (Immutable)

### Compile and run

    ./compile.sh src/TestLinkedList.java && ./run.sh src/TestLinkedList

### Don't output traces

    ./run.sh src/TestLinkedList 2> /dev/null
