package src;
import java.util.ArrayList;
import java.util.Collections;
import java.util.function.Consumer;


/**
 * LinkedList abstract class.
 */
public abstract class LinkedList implements Iterable<LinkedList>,
                                            Comparable<LinkedList>
{
    /**
     *  Factory
     */
    public static LinkedList makeNode(String value)
    {
        return new LinkedListNode(value);
    }

    public static LinkedList makeNode(String value, LinkedList tail)
    {
        return new LinkedListNode(value, tail);
    }

    public static LinkedList makeList(String... values)
    {
        LinkedList list = new EmptyNode();
        for (String value : values) {
            list = list.insert(value, list.size());
        }
        return list;
    }

    public static LinkedList makeList()
    {
        return new EmptyNode();
    }

    public abstract LinkedList head();
    public abstract LinkedList tail();

    public abstract String value();

    public abstract int size();
    public abstract boolean isEmpty();

    public abstract String join();
    public abstract String join(String separator);

    public abstract LinkedList copy();
    public abstract LinkedList concat(LinkedList list);
    public abstract LinkedList unique();

    public abstract LinkedList search(String value);

    public abstract LinkedList slice(int start, int count);
    public abstract LinkedList append(String value);
    public abstract LinkedList insert(String value, int position);
    public abstract LinkedList insert(LinkedList list, int position);
    public abstract LinkedList invert();

    public abstract LinkedList intersection(LinkedList list);
    public abstract LinkedList union(LinkedList list);
    public abstract boolean includes(LinkedList list);

    @Override
    public abstract String toString();

    /**
     *  Ordering
     */
    public abstract LinkedList lesser(String value);
    public abstract LinkedList greater(String value);

    /**
     *  Sorting
     */
    public abstract LinkedList quicksort();

    /**
     *  Iterable Interface
     */
    @Override
    public LinkedListIterator iterator() {
        return new LinkedListIterator(this);
    }

    @Override
    public void forEach(Consumer<? super LinkedList> action)
    {
        for (LinkedList node : this) {
            action.accept(node);
        }
    }

    /**
     *  Comparable Interface
     */
    @Override
    public int compareTo(LinkedList list)
    {
        return this.value().compareTo(list.value());
    }

    /**
     *  Mutation
     */
    public abstract void setValue(String value);

    /**
     *  Internal methods.
     */
    protected abstract LinkedList _slice(int start, int count, int position);
    protected abstract LinkedList _insert(LinkedList head, LinkedListNode node, int rankCount);
    protected abstract LinkedList _invert(ArrayList<String> values);
    protected abstract LinkedList _unique(LinkedList uniqueList);
    protected abstract LinkedList _intersection(LinkedList keys, LinkedList inter);

}




class EmptyNode extends LinkedList
{
    @Override
    public EmptyNode head() {
        return this;
    }

    @Override
    public EmptyNode tail() {
        return this;
    }


    @Override
    public String value() {
        return null;
    }


    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return true;
    }


    @Override
    public String join() {
        return null; /* not supposed to be reached */
    }

    @Override
    public String join(String separator) {
        return null; /* not supposed to be reached */
    }


    @Override
    public LinkedList copy() {
        return this;
    }

    @Override
    public LinkedList concat(LinkedList list) {
        return list.copy();
    }

    @Override
    public LinkedList unique() {
        return this;
    }


    @Override
    public LinkedList search(String value) {
        return this;
    }


    @Override
    public LinkedList append(String value) {
        return LinkedList.makeNode(value);
    }

    @Override
    public LinkedList slice(int start, int count) {
        return this;
    }

    @Override
    public LinkedList insert(String value, int position) {
        return LinkedList.makeNode(value);
    }

    @Override
    public LinkedList insert(LinkedList list, int position) {
        return list.copy();
    }


    @Override
    public LinkedList invert() {
        return this;
    }


    @Override
    public LinkedList intersection(LinkedList list) { return this; }

    @Override
    public LinkedList union(LinkedList list)
    {
        return list.unique();
    }

    @Override
    public boolean includes(LinkedList list) {
        return list.isEmpty();
    }

    /**
     *  Ordering
     */
    @Override
    public LinkedList lesser(String value) {
        return this;
    }

    @Override
    public LinkedList greater(String value) {
        return this;
    }



    /**
     *  Sorting
     */
    @Override
    public LinkedList quicksort() {
        return this;
    }


    /**
     * Mutation
     */
    @Override
    public void setValue(String value) { /* unimplemented */ }


    /**
     *  Object Interface
     */
    @Override
    public String toString() {
        return "[]";
    }


    /**
     * Internal methods.
     */
    @Override
    protected LinkedList _slice(int start, int count, int position) {
        return this;
    }

    @Override
    protected LinkedList _insert(LinkedList head, LinkedListNode node, int rankCount) {
        return this;
    }

    @Override
    protected LinkedList _invert(ArrayList<String> values) {
        return this;
    }

    @Override
    protected LinkedList _unique(LinkedList uniqueList) {
        return uniqueList;
    }

    @Override
    protected LinkedList _intersection(LinkedList keys, LinkedList inter) {
        return inter;
    }
}




class LinkedListNode extends LinkedList
{
    LinkedList _tail;
    String     _value;

    LinkedListNode(String value) {
        this._tail = new EmptyNode();
        this._value = value;
    }

    LinkedListNode(String value, LinkedList tail) {
        this._tail = tail;
        this._value = value;
    }

    @Override
    public LinkedList head() {
        return this;
    }

    @Override
    public LinkedList tail() {
        return this._tail;
    }

    @Override
    public String value() {
        return this._value;
    }

    @Override
    public int size() {
        return 1 + this.tail().size();
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public String join() {
        return this._tail.isEmpty()
            ? this._value
            : this._value + this._tail.join();
    }

    @Override
    public String join(String separator) {
        return this._tail.isEmpty()
            ? this._value
            : this._value + separator + this._tail.join(separator);
    }


    @Override
    public LinkedList copy() {
        System.err.println("<trace> "+this+".copy()");
        return new LinkedListNode(this._value, this._tail.copy());
    }

    @Override
    public LinkedList concat(LinkedList list) {
        System.err.println("<trace> "+this+".concat("+list+")");
        return new LinkedListNode(this._value, this._tail.concat(list));
    }


    @Override
    public LinkedList unique() {
        return this._unique(new EmptyNode());
    }

    @Override
    protected LinkedList _unique(LinkedList uniqueList)
    {
        System.err.println("<trace> "+this+"._unique("+uniqueList+")");
        if (uniqueList.search(this.value()).isEmpty()) {
            uniqueList = uniqueList.concat(LinkedList.makeNode(this.value()));
        }
        return this._tail._unique(uniqueList);
    }


    @Override
    public LinkedList search(String value) {
        return this._value.equals(value)
            ? this
            : this._tail.search(value);
    }


    @Override
    public LinkedList append(String value) {
        return this.concat(LinkedList.makeNode(value));
    }

    @Override
    public LinkedList slice(int start, int count) {
        return this._slice(start, count, 0);
    }

    @Override
    protected LinkedList _slice(int start, int count, int position) {
        System.err.println("<trace> _slice("+start+", "+count+", "+position+")");
        if (position < start) {
            return this._tail._slice(start, count, position+1);
        }
        else if (position < (start+count)) {
            return new LinkedListNode(this._value, this._tail._slice(start, count, position+1));
        }
        else {
            return new EmptyNode();
        }
    }


    @Override
    public LinkedList insert(String value, int position)
    {
        LinkedListNode node = new LinkedListNode(value);
        if (position == 0) {
            node._tail = this.copy();
            return node;
        }
        else {
            LinkedList copy = this.copy();
            return copy._insert(copy, node, position-1);
        }
    }

    @Override
    protected LinkedList _insert(LinkedList head, LinkedListNode node, int rankCount)
    {
        if (rankCount == 0 || this._tail.isEmpty()) {
            node._tail = this._tail;
            this._tail = node;
            return head;
        }
        else {
            return this._tail._insert(head, node, rankCount-1);
        }
    }


    @Override
    public LinkedList insert(LinkedList list, int position) {
        final LinkedList left = this.slice(0, position);
        return left
            .concat(list)
            .concat(this.slice(position, this.size()));
    }


    @Override
    public LinkedList invert() {
        return this._invert(new ArrayList<String>());
    }


    @Override
    protected LinkedList _invert(ArrayList<String> values)
    {
        values.add(this.value());

        if (!this.tail().isEmpty()) {
            return this.tail()._invert(values);
        }
        else {
            Collections.reverse(values);
            LinkedList list = LinkedList.makeNode(values.get(0));
            values.remove(0);
            for (final String value : values) {
                list = list.concat(LinkedList.makeNode(value));
            }
            return list;
        }
    }



    @Override
    public LinkedList intersection(LinkedList keys)
    {
        return this._intersection(keys, LinkedList.makeList());
    }

    @Override
    protected LinkedList _intersection(LinkedList keys, LinkedList inter)
    {
        return !keys.search(this._value).isEmpty()
            ? this._tail._intersection(keys, inter.append(this._value))
            : this._tail._intersection(keys, inter);
    }

    @Override
    public LinkedList union(LinkedList list)
    {
        return this.concat(list).unique();
        /* alternate implementation, in its own terms
           (needs the current specific implementation of EmptyNode.union) */
        // return this._tail.union(list.append(this._value));
    }


    @Override
    public boolean includes(LinkedList keys)
    {
        for (LinkedList key : keys) {
            if (this.search(key.value()).isEmpty()) {
                return false;
            }
        }
        return true;
    }



    /**
     *  Ordering
     */
    @Override
    public LinkedList lesser(String key)
    {
        return (this._value.compareTo(key) < 0)
            ? LinkedList.makeNode(this._value, this._tail.lesser(key))
            : this._tail.lesser(key);
    }

    @Override
    public LinkedList greater(String key)
    {
        return (this._value.compareTo(key) > 0)
            ? LinkedList.makeNode(this._value, this._tail.greater(key))
            : this._tail.greater(key);
    }


    /**
     *  Sorting
     */
    @Override
    public LinkedList quicksort()
    {
        System.out.println("<QS> "+this);
        return this.lesser(this._value)
            .append(this._value)
            .concat(this.greater(this._value));
    }


    /**
     * Mutation
     */
    @Override
    public void setValue(String value)
    {
        this._value = value;
    }


    /**
     *  Object interface
     */
    @Override
    public String toString() {
        return "[ "+this.join(" | ")+" ]";
    }

}
