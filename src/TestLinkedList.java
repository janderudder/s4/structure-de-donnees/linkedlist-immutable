package src;


public class TestLinkedList
{
    public static void main(String args[])
    {
        LinkedList emptyList = LinkedList.makeList();
        System.out.println("emptyList = LinkedList.makeList();");
        System.out.println("-> "+emptyList);

        LinkedList list = LinkedList.makeNode("One", LinkedList.makeNode("Two"));
        System.out.println("\nlist = LinkedList.makeNode(\"One\", LinkedList.makeNode(\"Two\"));");
        System.out.println("-> "+list);

        LinkedList secondList = LinkedList.makeList("Three", "Four");
        System.out.println("\nsecondList = LinkedList.makeList(\"Three\", \"Four\");");
        System.out.println("-> "+secondList);

        /**
         *  Concatenation
         */
        System.out.println("\nConcatenation");
        System.out.println("----------------------------------");
        list = list.concat(secondList);
        System.out.println("list = list.concat(secondList);");
        System.out.println("-> "+list);

        /**
         *  Size
         */
        System.out.println("\nSize");
        System.out.println("----------------------------------");
        System.out.println("list.size(): "+list.size());
        System.out.println("list.tail().size(): "+list.tail().size());
        System.out.println("list.tail().tail().size(): "+list.tail().tail().size());
        System.out.println("list.tail().tail().tail().size(): "+list.tail().tail().tail().size());
        System.out.println("list.tail().tail().tail().tail().size(): "+list.tail().tail().tail().tail().size());
        System.out.println("list.tail().tail().tail().tail().tail().size(): "+list.tail().tail().tail().tail().tail().size()+"     # beyond last node");

        /**
         *  Slice
         */
        System.out.println("\nSlice");
        System.out.println("----------------------------------");
        LinkedList slice = list.slice(0, 0);
        System.out.println("slice(0, 0): "+slice);

        slice = list.slice(0, 1);
        System.out.println("slice(0, 1): "+slice);

        slice = list.slice(0, 2);
        System.out.println("slice(0, 2): "+slice);

        slice = list.slice(0, 3);
        System.out.println("slice(0, 3): "+slice);

        slice = list.slice(0, 4);
        System.out.println("slice(0, 4): "+slice);

        slice = list.slice(1, 1);
        System.out.println("slice(1, 1): "+slice);

        slice = list.slice(1, 2);
        System.out.println("slice(1, 2): "+slice);

        slice = list.slice(1, 3);
        System.out.println("slice(1, 3): "+slice);

        slice = list.slice(2, 1);
        System.out.println("slice(2, 1): "+slice);

        slice = list.slice(2, 2);
        System.out.println("slice(2, 2): "+slice);

        slice = list.slice(3, 1);
        System.out.println("slice(3, 1): "+slice);

        System.out.println("tail.slice(0, 0): "+list.tail().slice(0, 0));
        System.out.println("tail.slice(0, 1): "+list.tail().slice(0, 1));
        System.out.println("tail.slice(0, 2): "+list.tail().slice(0, 2));
        System.out.println("tail.slice(0, 3): "+list.tail().slice(0, 3));

        System.out.println("tail.tail.slice(0, 1): "+list.tail().tail().slice(0, 1));
        System.out.println("tail.tail.slice(0, 2): "+list.tail().tail().slice(0, 2));
        System.out.println("tail.tail.slice(0, 3): "+list.tail().tail().slice(0, 3)+"            # beyond last node");

        /**
         *  Search
         */
        System.out.println("\nSearch");
        System.out.println("----------------------------------");
        System.out.println("list.search(\"One\"): "+list.search("One"));
        System.out.println("list.search(\"Three\"): "+list.search("Three"));
        System.out.println("list.search(\"Five\"): "+list.search("Five"));
        System.out.println("list.search(\"One\").value(): "+list.search("One").value());
        System.out.println("list.search(\"Five\").value(): "+list.search("Five").value());
        System.out.println("list.search(null): "+list.search(null));
        System.out.println("list.search(null).value(): "+list.search(null).value());

        /**
         *  Unique
         */
        System.out.println("\nUnique");
        System.out.println("----------------------------------");
        System.out.println(list.concat(list)+".unique(): "+list.concat(list).unique());

        /**
         *  Append
         */
        System.out.println("\nAppend");
        System.out.println("----------------------------------");
        System.out.println(emptyList+".append(\"One\"): "+emptyList.append("One"));
        System.out.println(list+".append(\"Five\"): "+list.append("Five"));

        /**
         *  Insert
         */
        System.out.println("\nInsert(String)");
        System.out.println("----------------------------------");
        System.out.println(list+".insert(\"Zero\", 0): "+list.insert("Zero", 0));
        System.out.println(list+".insert(\"1.5\", 1): "+list.insert("1.5", 1));
        System.out.println(list+".insert(\"2.5\", 2): "+list.insert("2.5", 2));
        System.out.println(list+".insert(\"3.5\", 3): "+list.insert("3.5", 3));
        System.out.println(list+".insert(\"5\", 4): "+list.insert("5", 4));
        System.out.println(list+".insert(\"5\", 5): "+list.insert("5", 5)+"     # beyond last node");

        System.out.println("\nInsert(LinkedList)");
        System.out.println("----------------------------------");
        System.out.println(list+".insert("+LinkedList.makeList("Elem1", "Elem2")+", 0): "+list.insert(LinkedList.makeList("Elem1", "Elem2"), 0));
        System.out.println(list+".insert("+LinkedList.makeList("Elem1", "Elem2")+", 2): "+list.insert(LinkedList.makeList("Elem1", "Elem2"), 2));
        System.out.println(list+".insert("+LinkedList.makeList("Elem1", "Elem2")+", 4): "+list.insert(LinkedList.makeList("Elem1", "Elem2"), 4));

        /**
         *  Invert
         */
        System.out.println("\nInvert");
        System.out.println("----------------------------------");
        System.out.println("list.invert(): "+list.invert());
        System.out.println("list.tail().invert(): "+list.tail().invert());
        System.out.println("list.tail().tail().invert(): "+list.tail().tail().invert());
        System.out.println("list.tail().tail().tail().invert(): "+list.tail().tail().tail().invert());
        System.out.println("list.tail().tail().tail().tail().invert(): "+list.tail().tail().tail().tail().invert()+"            # invert empty node");
        System.out.println("list.tail().tail().tail().tail().tail().invert(): "+list.tail().tail().tail().tail().tail().invert()+"     # beyond last node");

        /**
         *  Includes
         */
        System.out.println("\nIncludes");
        System.out.println("----------------------------------");
        System.out.println(emptyList+".includes("+emptyList+"): "+emptyList.includes(emptyList));
        System.out.println(list+".includes("+emptyList+"): "+list.includes(emptyList));
        System.out.println(list+".includes("+list+"): "+list.includes(list));
        System.out.println(list+".includes("+list.slice(0, 1)+"): "+list.includes(list.slice(0, 1)));
        System.out.println(list+".includes("+list.slice(1, 1)+"): "+list.includes(list.slice(0, 1)));
        System.out.println(list+".includes("+list.slice(1, 2)+"): "+list.includes(list.slice(0, 1)));
        System.out.println(list+".includes("+list.slice(1, 3)+"): "+list.includes(list.slice(1, 3)));
        System.out.println(list+".includes("+list.slice(2, 2)+"): "+list.includes(list.slice(2, 2)));
        System.out.println(list+".includes("+list.slice(3, 1)+"): "+list.includes(list.slice(2, 2)));
        System.out.println(list.concat(LinkedList.makeList("Five", "Six"))+".includes("+list+"): "+list.concat(LinkedList.makeList("Five", "Six")).includes(list));
        System.out.println(list+".includes("+list.concat(LinkedList.makeList("Five", "Six"))+"): "+list.includes(list.concat(LinkedList.makeList("Five", "Six"))));
        System.out.println(list+".includes("+LinkedList.makeList("Five", "Six")+"): "+list.includes(LinkedList.makeList("Five", "Six")));

        /**
         *  Intersection
         */
        System.out.println("\nIntersection");
        System.out.println("----------------------------------");
        System.out.println(emptyList+".intersection("+emptyList+"): "+emptyList.intersection(emptyList));
        System.out.println(list+".intersection("+emptyList+"): "+list.intersection(emptyList));
        System.out.println(list+".intersection("+LinkedList.makeNode("One")+"): "+list.intersection(LinkedList.makeNode("One")));
        System.out.println(list+".intersection("+secondList+"): "+list.intersection(secondList));

        /**
         *  Union
         */
        System.out.println("\nUnion");
        System.out.println("----------------------------------");
        System.out.println(list+".union("+emptyList+"): "+list.union(emptyList));
        System.out.println(list+".union("+secondList+"): "+list.union(secondList));
        System.out.println(list+".union("+LinkedList.makeList("One", "Five", "Six")+"): "+list.union(LinkedList.makeList("One", "Five", "Six")));

        /**
         * Iterable
         */
        System.out.println("\nIterable");
        System.out.println("----------------------------------");
        for (LinkedList node : list) {
            System.out.print(node.value().toUpperCase()+" \\ ");
        }
        System.out.println("    # iterable for loop (toUpperCase())");
        list.forEach(node -> System.out.print(node.value().toLowerCase()+" \\ "));
        System.out.println("    # forEach loop (toLowerCase())");

        /**
         * Mutation
         */
        System.out.println("\nMutation (in place)");
        System.out.println("----------------------------------");
        {
            Integer i=0;
            for (LinkedList node : list) {
                node.setValue((++i).toString());
            }
        }
        System.out.println("for (LinkedList node : list) {");
        System.out.println("    node.setValue((++i).toString());");
        System.out.println("}");
        System.out.println("list: "+list);
    }
}
