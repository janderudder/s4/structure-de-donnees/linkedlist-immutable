package src;
import java.util.Iterator;


public class LinkedListIterator implements Iterator<LinkedList>
{
    private LinkedList node;

    public LinkedListIterator(LinkedList list) {
        this.node = list;
    }

    @Override
    public boolean hasNext() {
        return !this.node.isEmpty();
    }

    @Override
    public LinkedList next() {
        LinkedList node = this.node;
        this.node = this.node.tail();
        return node;
    }
}
