package src;


public class TestOrdering
{
    public static void main(String args[])
    {
        LinkedList emptyList = LinkedList.makeList();
        LinkedList lo = LinkedList.makeList("a", "d", "h", "n", "p");
        LinkedList lu = LinkedList.makeList("d", "n", "h", "p", "a");

        System.out.println(lo.lesser("h"));
        System.out.println(lo.greater("h"));
        System.out.println(lu.lesser("h"));
        System.out.println(lu.greater("h"));

        System.out.println(lo.lesser("a"));
        System.out.println(lo.greater("a"));
        System.out.println(lu.lesser("a"));
        System.out.println(lu.greater("a"));

        System.out.println(lo.lesser("m"));
        System.out.println(lo.greater("m"));
        System.out.println(lu.lesser("m"));
        System.out.println(lu.greater("m"));

        // LinkedList unordered = LinkedList.makeList("e", "c", "b", "a", "f", "h", "d", "g");
        // System.out.println(unordered+".quicksort(): "+unordered.quicksort());
    }
}
